package main

import (
	"fmt"
	"html"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os/exec"
	"regexp"
	"strconv"
	"strings"

	"github.com/rylio/ytdl"
)

var (
	r = regexp.MustCompile(`<a class="yt-uix-sessionlink yt-uix-tile-link  spf-link  yt-ui-ellipsis yt-ui-ellipsis-2" dir=".*" title="(.*?)" .* href="(.*)" rel="nofollow">`)
)

// ProgressReader ..
type ProgressReader struct {
	io.Reader
	Reporter func(r int64)
}

func (pr *ProgressReader) Read(p []byte) (n int, err error) {
	n, err = pr.Reader.Read(p)
	pr.Reporter(int64(n))
	return
}

func main() {
	log.SetFlags(log.Lshortfile)

	resp, err := http.Get("https://www.youtube.com/user/PandoraMuslc/videos")
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	videos := r.FindAllStringSubmatch(string(body), -1)

	//var datum = make(chan *bytes.Buffer)
	//throttle := make(chan struct{})

	for _, v := range videos {

		id := strings.Split(v[2], "?v=")[1]

		fmt.Printf(
			"Loading: %s\n\n",
			html.UnescapeString(v[1]),
		)

		var (
			format ytdl.Format
		)

		video, err := ytdl.GetVideoInfoFromID(id)
		if err != nil {
			log.Println(err)
			return
		}

		for _, fmt := range video.Formats {
			if fmt.Itag == 250 { // opus, 70kbps
				format = fmt
			}
		}

		u, err := video.GetDownloadURL(format)
		if err != nil {
			log.Println(err)
			return
		}

		req, err := http.NewRequest("GET", u.String(), nil)
		if err != nil {
			log.Println(err)
		}

		resp, err := (&http.Client{}).Do(req)
		if err != nil {
			log.Println(err)
		}

		if resp.StatusCode < 200 || resp.StatusCode > 299 {
			log.Printf("Invalid status code: %d\n", resp.StatusCode)
			return
		}

		total := func() int64 {
			contentLength := resp.Header.Get("Content-Length")
			if contentLength == "" {
				return 0
			}

			i, e := strconv.ParseInt(contentLength, 10, 64)
			if e != nil {
				log.Panicln("They told me not to ignore the error, so", err)
			}

			return i
		}()

		var current int64

		pr := &ProgressReader{resp.Body, func(r int64) {
			current += r

			print("\033[1A")
			fmt.Printf("\033[0K\rProgress: %d%% (%d/%d)\n", current*100/total, current, total)
		}}

		play(pr)

		resp.Body.Close()

		//datum <- buffer

	}

	//for {
	//buffer, ok := <-datum
	//if !ok {
	//break
	//} else {
	//play(buffer)
	//}
	//}
}

func play(body io.Reader) {
	trans := exec.Command("mpv", "-")

	stdin, err := trans.StdinPipe()
	if err != nil {
		panic(err)
	}

	if err := trans.Start(); err != nil {
		panic(err)
	}

	if _, err := io.Copy(stdin, body); err != nil {
		panic(err)
	}

	stdin.Close()

	if err := trans.Wait(); err != nil {
		panic(err)
	}

}
